package com.adionisio.rsskotlinreader

/**
 * Created by Alejandro on 01/08/2018.
 */

interface BasePresenter {
    fun start()
    fun stop()
}