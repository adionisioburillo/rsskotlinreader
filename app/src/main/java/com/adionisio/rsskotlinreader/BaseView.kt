package com.adionisio.rsskotlinreader

import android.content.Context

/**
 * Created by Alejandro on 01/08/2018.
 */

interface BaseView<T> {
    fun setPresenter(presenter: T)
}