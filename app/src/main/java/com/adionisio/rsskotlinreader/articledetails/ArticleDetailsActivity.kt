package com.adionisio.rsskotlinreader.articledetails

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.adionisio.rsskotlinreader.R
import com.adionisio.rsskotlinreader.data.Article
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_article_details.*

class ArticleDetailsActivity : AppCompatActivity(), ArticleDetailsContract.View {

    private lateinit var mpresenter: ArticleDetailsContract.Presenter
    private lateinit var title: TextView
    private lateinit var description: TextView
    private lateinit var button: Button
    private lateinit var image: ImageView
    private lateinit var url: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article_details)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = findViewById(R.id.textViewTitleDetails)
        description = findViewById(R.id.textViewDescriptionDetails)
        button = findViewById(R.id.buttonWeb)
        button.setOnClickListener(View.OnClickListener { viewArticleInBrowser() })
        image = findViewById(R.id.imageViewDetails)
        val article: Article = intent.getSerializableExtra(ARTICLE) as Article
        ArticleDetailsPresenter(this)
        mpresenter.setArticle(article)

    }

    override fun onResume() {
        super.onResume()
        mpresenter.start()
    }

    override fun setPresenter(presenter: ArticleDetailsContract.Presenter) {
        mpresenter = presenter
    }

    override fun setTitle(title: String) {
        this.title.text = title
    }

    override fun setDescription(description: String) {
        this.description.text = description
    }

    override fun setImage(imageURL: String) {
        Picasso.with(baseContext).load(imageURL).into(this.image)
    }

    override fun setButtonURL(urlArticleLink: String) {
        this.url = urlArticleLink
    }

    companion object {

        private val ARTICLE = "article_selected"

        fun newIntent(context: Context, article: Article): Intent {
            val intent = Intent(context, ArticleDetailsActivity::class.java)
            intent.putExtra(ARTICLE, article)
            return intent
        }
    }

    private fun viewArticleInBrowser() {
        val uris = Uri.parse(url)
        val intents = Intent(Intent.ACTION_VIEW, uris)
        startActivity(intents)
    }


}
