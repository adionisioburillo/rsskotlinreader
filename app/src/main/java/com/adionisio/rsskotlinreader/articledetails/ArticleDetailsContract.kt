package com.adionisio.rsskotlinreader.articledetails

import com.adionisio.rsskotlinreader.BasePresenter
import com.adionisio.rsskotlinreader.BaseView
import com.adionisio.rsskotlinreader.data.Article
import java.util.ArrayList

/**
 * Created by Alejandro on 02/08/2018.
 */
interface ArticleDetailsContract {
    interface View : BaseView<Presenter> {

        fun setTitle(title: String)
        fun setDescription(description: String)
        fun setImage(imageURL: String)
        fun setButtonURL(urlArticleLink: String)
    }

    interface Presenter : BasePresenter {
        fun setArticle(article: Article)
    }
}