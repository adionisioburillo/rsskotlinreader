package com.adionisio.rsskotlinreader.articledetails

import com.adionisio.rsskotlinreader.data.Article

/**
 * Created by Alejandro on 02/08/2018.
 */
class ArticleDetailsPresenter(var view: ArticleDetailsContract.View) : ArticleDetailsContract.Presenter {

    private lateinit var article: Article

    init {
        view.setPresenter(this)
    }

    override fun start() {
        view.setTitle(article.title!!)
        view.setDescription(article.description!!)
        view.setButtonURL(article.url!!)
        view.setImage(article.urlToImage!!)
    }

    override fun stop() {
    }

    override fun setArticle(article: Article) {
        this.article = article
    }
}