package com.adionisio.rsskotlinreader.articleslist

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.LinearLayout
import android.widget.Toast
import com.adionisio.rsskotlinreader.R
import com.adionisio.rsskotlinreader.articledetails.ArticleDetailsActivity
import com.adionisio.rsskotlinreader.data.Article
import com.adionisio.rsskotlinreader.articleslist.ArticlesListContract.View
import com.adionisio.rsskotlinreader.articleslist.adapter.ArticleAdapter
import kotlinx.android.synthetic.main.activity_articles_list.*
import java.util.ArrayList

class ArticlesListActivity : AppCompatActivity(), View {

    private lateinit var mpresenter: ArticlesListContract.Presenter
    private lateinit var loading: LinearLayout
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_articles_list)
        setSupportActionBar(toolbar)
        loading = findViewById(R.id.layoutLoading)
        recyclerView = findViewById(R.id.rv_articles)
        recyclerView.layoutManager = LinearLayoutManager(this)

        ArticlesListPresenter(this)
    }

    override fun onResume() {
        super.onResume()
        mpresenter.start()
    }

    override fun showLoading() {
        loading.visibility = android.view.View.VISIBLE
    }

    override fun hideLoading() {
        loading.visibility = android.view.View.GONE
    }

    override fun sendFeedback(title: String) {
        Snackbar.make(findViewById(android.R.id.content), title, Snackbar.LENGTH_SHORT)
                .show()
    }

    override fun showArticles(articlesList: ArrayList<Article>?) {
        recyclerView.adapter = ArticleAdapter(articlesList, this, { article: Article -> articleItemClicked(article) })
    }

    override fun setPresenter(presenter: ArticlesListContract.Presenter) {
        mpresenter = presenter
    }

    override fun getContext(): Context = applicationContext

    override fun onDestroy() {
        super.onDestroy()
        mpresenter.stop()
    }

    private fun articleItemClicked(articleItem: Article) {
        val intent = ArticleDetailsActivity.newIntent(this, articleItem)
        startActivity(intent)
    }


}
