package com.adionisio.rsskotlinreader.articleslist

import android.content.Context
import com.adionisio.rsskotlinreader.BasePresenter
import com.adionisio.rsskotlinreader.BaseView
import com.adionisio.rsskotlinreader.data.Article
import java.util.ArrayList

/**
 * Created by Alejandro on 01/08/2018.
 */
interface ArticlesListContract {
    interface View : BaseView<Presenter> {
        fun showLoading()
        fun hideLoading()
        fun sendFeedback(title: String)
        fun showArticles(articlesList: ArrayList<Article>?)
        fun getContext(): Context
    }

    interface Presenter : BasePresenter {
    }
}