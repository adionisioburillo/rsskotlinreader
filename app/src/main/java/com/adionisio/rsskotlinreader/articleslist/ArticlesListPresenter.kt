package com.adionisio.rsskotlinreader.articleslist

import com.adionisio.rsskotlinreader.provider.RssService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by Alejandro on 01/08/2018.
 */
class ArticlesListPresenter(var view: ArticlesListContract.View?) : ArticlesListContract.Presenter {

    var disposable: Disposable? = null

    init {
        this.view?.setPresenter(this)
    }

    override fun start() {
        getArticles()
    }

    override fun stop() {
        view = null
        disposable?.dispose()
    }

    private fun getArticles() {
        val rssService = RssService()

        disposable = rssService.getRssArticles("metro").subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            view?.showArticles(result.articles)
                            view?.hideLoading()
                        },
                        { error -> view?.sendFeedback("ERROR EN EL OBSERVER" + error.message!!) }
                )

    }
}