package com.adionisio.rsskotlinreader.articleslist.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adionisio.rsskotlinreader.R
import com.adionisio.rsskotlinreader.data.Article
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_article.view.*
import java.util.ArrayList

/**
 * Created by Alejandro on 01/08/2018.
 */
class ArticleAdapter(val articles: ArrayList<Article>?, val context: Context, val clickListener: (Article) -> Unit) : RecyclerView.Adapter<ArticleAdapter.ArticleViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ArticleViewHolder {
        return ArticleViewHolder(LayoutInflater.from(context).inflate(R.layout.item_article, parent, false))
    }

    override fun getItemCount(): Int = articles!!.size

    override fun onBindViewHolder(holder: ArticleViewHolder?, position: Int) {
        (holder as ArticleViewHolder).bind(articles!![position], clickListener, context)

    }

    class ArticleViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title = view.textViewTitle
        val description = view.textViewDescription
        val image = view.imageViewArticle

        fun bind(article: Article, clickListener: (Article) -> Unit, context: Context) {
            title.text = article.title
            description.text = article.description
            Picasso.with(context).load(article.urlToImage).fit().centerCrop().into(image)
            itemView.setOnClickListener { clickListener(article) }
        }

    }

}