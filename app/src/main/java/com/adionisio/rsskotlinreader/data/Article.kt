package com.adionisio.rsskotlinreader.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Alejandro on 01/08/2018.
 */
class Article : Serializable {
    @SerializedName("author")
    @Expose
    private val author: String? = null
    @SerializedName("title")
    @Expose
    val title: String? = null
    @SerializedName("description")
    @Expose
    val description: String? = null
    @SerializedName("url")
    @Expose
    val url: String? = null
    @SerializedName("urlToImage")
    @Expose
    val urlToImage: String? = null
    @SerializedName("publishedAt")
    @Expose
    val publishedAt: String? = null

}