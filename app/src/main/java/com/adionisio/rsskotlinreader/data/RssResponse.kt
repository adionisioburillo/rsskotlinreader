package com.adionisio.rsskotlinreader.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Alejandro on 01/08/2018.
 */

class RssResponse {
    @SerializedName("status")
    @Expose
    private val status: String? = null
    @SerializedName("totalResults")
    @Expose
    private val totalResults: Int? = null
    @SerializedName("articles")
    @Expose
    val articles: ArrayList<Article>? = null

}