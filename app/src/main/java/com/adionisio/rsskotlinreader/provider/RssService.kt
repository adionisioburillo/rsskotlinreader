package com.adionisio.rsskotlinreader.provider

import com.adionisio.rsskotlinreader.data.RssResponse
import com.adionisio.rsskotlinreader.utils.Config
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Alejandro on 01/08/2018.
 */
class RssService() {

    private var serviceApi: RssServiceApi

    init {
        val gson: Gson = GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create()
        val retrofit: Retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(Config.RSSSERVER)
                .build()

        serviceApi = retrofit.create(RssServiceApi::class.java)
    }

    fun getRssArticles(source: String): Observable<RssResponse> {
        return serviceApi.getNews(Config.APIKEY, source)
    }

}