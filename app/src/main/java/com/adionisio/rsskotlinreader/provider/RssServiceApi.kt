package com.adionisio.rsskotlinreader.provider

import com.adionisio.rsskotlinreader.data.RssResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Alejandro on 01/08/2018.
 */
interface RssServiceApi {
    @GET("/v2/top-headlines")
    fun getNews(
            @Query("apiKey") apiKey: String,
            @Query("sources") sources: String
    ): Observable<RssResponse>
}